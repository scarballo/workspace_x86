#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT      27  // ASCII para la tecla Esc
#define SENSOR_FICHA  49  // ASCII para la tecla 1
#define BOTON_CAFE  50  // ASCII para la tecla 2
#define BOTON_TE  51  // ASCII para la tecla 3

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Funciones que configuran la consola de Linux como interfaz de I/O
void hw_Init(void);
void hw_DeInit(void);

// Funciones basicas para leer que entrada esta activa y pausar la ejecucion
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);

void hw_ParpadeoLed1Hz(void);
void hw_ParpadeoLed5Hz(void);
void hw_DevolverFicha(void);
void hw_RealizarCafe(void);
void hw_RealizarTe(void);
void hw_FinCafe(void);
void hw_FinTe(void);
void hw_Alarma_ON(void);
void hw_Alarma_OFF(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
