/*==================[inclusions]=============================================*/

#include "fsm_cafetera.h"
#include "hw.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

#define ESPERA_SELECCIONANDO_SEG  10
#define ESPERA_CAFE_SEG  10
#define ESPERA_TE_SEG  10
#define ESPERA_ALARMA_SEG  2

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

FSM_CAFETERA_STATES_T state;

bool evSensorFicha_ON_raised;
bool evSensorFicha_OFF_raised;
bool evBotonCafe_raised;
bool evBotonTe_raised;
bool evTick1seg_raised;
bool evTick100mseg_raised;
uint8_t count_seg;
uint8_t count_mseg;
uint8_t count_restante;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clearEvents(void)
{
    evSensorFicha_ON_raised = 0;
    evSensorFicha_OFF_raised = 0;
    evBotonCafe_raised = 0;
    evBotonTe_raised = 0;
    evTick1seg_raised = 0;
    //evTick100mseg_raised = 0;
}

/*==================[external functions definition]==========================*/

void fsm_cafetera_init(void)
{
    state = REPOSO;
    printf ("\nIngrese Ficha (1)\n\n");
    clearEvents();
}

void fsm_cafetera_runCycle(void)
{
    // El diagrama se encuentra en TP2/media/TP2 - Diagrama de estado.png
    switch (state) {
        case REPOSO:
            if (evSensorFicha_ON_raised) {
                count_seg = 0;
                count_mseg = 0;
                printf ("Seleccione Bebida: \n 2: Cafe \n 3: Te \n\n");
                state = SELECCIONANDO;
            }
            else if (evSensorFicha_OFF_raised) {            
                
                hw_ParpadeoLed1Hz(); 
                          
            }

            break;

        case SELECCIONANDO:
            if (evBotonCafe_raised) {
                count_seg = 0;
                count_mseg = 0;
                hw_ParpadeoLed5Hz();
                hw_RealizarCafe();
                
                state = CAFE;
            }
            else if (evBotonTe_raised) {
                count_seg = 0;
                count_mseg = 0;
                
                hw_RealizarTe();
                state = TE;
            }
            else if (evTick1seg_raised && (count_seg < ESPERA_SELECCIONANDO_SEG)) {
                count_seg++;
                count_restante = ESPERA_SELECCIONANDO_SEG - count_seg + 1;
                printf("Tiempo restante: %0d \n", count_restante);

            }
            else if (evTick1seg_raised && (count_seg == ESPERA_SELECCIONANDO_SEG)) {
                count_seg = 0;
                count_mseg = 0;
                count_restante = 0;
                hw_DevolverFicha();
                printf ("\nIngrese Ficha (1)\n\n");
                state = REPOSO;
            }
            break;

        case CAFE:
            hw_ParpadeoLed5Hz();
            if (evTick1seg_raised && (count_seg < ESPERA_CAFE_SEG)) {
                count_seg++;
                count_restante = ESPERA_CAFE_SEG - count_seg + 1;
                printf("Tiempo para su Cafe: %0d \n", count_restante);
                
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_CAFE_SEG)) {
                count_seg = 0;
                count_restante = 0;
                hw_FinCafe();
                hw_Alarma_ON();
                state = ALARMA;
            }
            break;

        case TE:
            hw_ParpadeoLed5Hz();
            if (evTick1seg_raised && (count_seg < ESPERA_TE_SEG)) {
                count_seg++;
                count_restante = ESPERA_CAFE_SEG - count_seg + 1;
                printf("Tiempo para su Te: %0d \n", count_restante);
                
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_TE_SEG)) {
                count_seg = 0;
                count_restante = 0;
                hw_FinTe();
                hw_Alarma_ON();
                state = ALARMA;
            }
            break;

        case ALARMA:
           if (evTick1seg_raised && (count_seg < ESPERA_ALARMA_SEG)) {
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_ALARMA_SEG)) {
                count_seg = 0;
                hw_Alarma_OFF();
                printf ("\n\nIngrese Ficha (1)\n\n");
                state = REPOSO;
            }
            break;
    }

    clearEvents();
}

void fsm_cafetera_raise_evFicha_ON(void)
{
    evSensorFicha_ON_raised = 1;
}

void fsm_cafetera_raise_evFicha_OFF(void)
{
    evSensorFicha_OFF_raised = 1;
}

void fsm_cafetera_raise_evCafe(void)
{
    evBotonCafe_raised = 1;
}

void fsm_cafetera_raise_evTe(void)
{
    evBotonTe_raised = 1;
}

void fsm_cafetera_raise_evTick1seg(void)
{
    evTick1seg_raised = 1;
}

void fsm_cafetera_raise_evTick100mseg(void)
{
    evTick100mseg_raised = 1;
}

void fsm_cafetera_printCurrentState(void)
{
    printf("Estado actual: %0d \n", state);
}

/*==================[end of file]============================================*/
