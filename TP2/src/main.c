/*==================[inclusions]=============================================*/

#include "main.h"
#include "fsm_cafetera.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_ms = 0;
    uint16_t cont_s = 0;

    hw_Init();

    fsm_cafetera_init();



    while (input != EXIT) {
        input = hw_LeerEntrada();

        // En un microcontrolador estos eventos se generarian aprovechando las
        // interrupciones asociadas a los GPIO
        if (input == SENSOR_FICHA) {
            fsm_cafetera_raise_evFicha_ON();
        }
        else {
            fsm_cafetera_raise_evFicha_OFF();
        }

        if (input == BOTON_CAFE) {
            fsm_cafetera_raise_evCafe();
        }
        if (input == BOTON_TE) {
            fsm_cafetera_raise_evTe();
        }

        // En un microcontrolador esto se implementaria en un handler de
        // interrupcion asociado a un timer
        cont_ms++;
        if (cont_ms == 100) {
            cont_ms = 0;
            cont_s ++;
            fsm_cafetera_raise_evTick100mseg();
        }
        else if (cont_s == 10) {
            cont_s = 0;
            cont_ms = 0;
            fsm_cafetera_raise_evTick1seg();
        }
        fsm_cafetera_runCycle();

        // Esta funcion hace que el sistema no responda a ningun evento por
        // 1 ms. Funciones bloqueantes de este estilo no suelen ser aceptables
        // en sistemas baremetal.
        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
