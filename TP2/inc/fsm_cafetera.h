#ifndef _CAFETERA_H_
#define _CAFETERA_H_

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef enum {
    REPOSO,
    SELECCIONANDO,
    CAFE,
    TE,
    ALARMA
} FSM_CAFETERA_STATES_T;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Inicializacion y evaluacion de la FSM
void fsm_cafetera_init(void);
void fsm_cafetera_runCycle(void);

// Eventos
void fsm_cafetera_raise_evFicha_ON(void);
void fsm_cafetera_raise_evFicha_(void);
void fsm_cafetera_raise_evCafe(void);
void fsm_cafetera_raise_evTe(void);
void fsm_cafetera_raise_evTick1seg(void);
void fsm_cafetera_raise_evTick100mseg(void);

// Debugging
void fsm_cafetera_printCurrentState(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _CAFETERA_H_ */
