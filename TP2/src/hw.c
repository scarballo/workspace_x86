/*==================[inclusions]=============================================*/

#include "hw.h"
#include "fsm_cafetera.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

#define ESPERA_1Hz_SEG  1
#define ESPERA_5Hz_SEG  2

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;

bool evTick1seg_raised;
bool evTick100mseg_raised;
uint8_t count_seg;
uint8_t count_mseg;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

void hw_ParpadeoLed1Hz(void)
{
    if (evTick1seg_raised && (count_seg < ESPERA_1Hz_SEG)) {
                count_seg++;
    }
    else if (evTick1seg_raised && (count_seg == ESPERA_1Hz_SEG)) {
                count_seg = 0;
                printf("\nLed Parpadeando a 1Hz \n\n");
    }
}



void hw_ParpadeoLed5Hz(void)
{   
    if (evTick100mseg_raised && (count_mseg < ESPERA_5Hz_SEG)) {
                count_mseg++;
    }
    else if (evTick100mseg_raised && (count_mseg == ESPERA_5Hz_SEG)) {
                count_mseg = 0;
                printf("\nLed Parpadeando a 5Hz \n\n");
    }    
}

void hw_DevolverFicha(void)
{
    printf("\nDevolviendo Ficha. Intente Nuevamente. \n\n");
}

void hw_RealizarCafe(void)
{
    printf("\nSu Cafe Esta En Proceso... \n\n");
}

void hw_RealizarTe(void)
{
    printf("\nSu Te Esta En Proceso... \n\n");
}

void hw_FinCafe(void)
{
    printf("\nSu Cafe Esta Listo. \n\n");
}

void hw_FinTe(void)
{
    printf("\nSu Te Esta Listo. \n\n");
}

void hw_Alarma_ON(void)
{
        printf("\nAlarma activada \n\n");
}

void hw_Alarma_OFF(void)
{
    printf("\nAlarma apagada \n\n");
}

/*==================[end of file]============================================*/